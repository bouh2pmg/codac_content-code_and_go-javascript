// On déroge un peut à la règle des consignes en anglais pour reposer les cerveaux

Cette demi-journée de JavaScript est destinée à vous mettre en appétit.
Découvrir la syntaxe et quelques aspects intéressants du langage.


-- Un point qui peut porter à confusion: ---
Node est utilisé pour exécuter le JavaScript, car comme vous le savez(ou pas),
le code JavaScript est destiné à être interprété par votre navigateur (bien qu'il n'en déplaise il est en réalité compilé).

L'utilisation de Node permet à votre code JavaScript de fonctionner sur la ligne de commande.


La seconde partie de la journée sera destinée à utiliser JavaScript dans le navigateur,
 là où il est maitre du comportement des pages.



Quelques liens:

La référence -- https://developer.mozilla.org/en-US/docs/Web/JavaScript
La référence -- https://developer.mozilla.org/fr/docs/Web/JavaScript

papa -- https://en.wikipedia.org/wiki/Brendan_Eich


  /\_/\
=( éwè )=
  )   (  //
 (__ __)//